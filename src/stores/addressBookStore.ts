import { ref } from 'vue'
import { defineStore } from 'pinia'
interface AddressBook{
  id: number
  name: string
  tel: string
  gender: string
}

export const useAddressBookStore = defineStore('address_book', () => {

let lastId = 1
const address = ref<AddressBook>({
  id: 0,
  name: '',
  tel: '',
  gender: 'Male'
})
const addressList = ref<AddressBook[]>([])
const isAddNew = ref(false)

function save(){
  // address.value.id = lastId++
  if(address.value.id>0){
    //edit
    const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
    addressList.value[editedIndex] = address.value
  }else{ // add new
    addressList.value.push({...address.value,id: lastId++}) //...คือระเบิดของเก่า แล้วเอาอันใหม่มาใส่ โดยให้ id++
  }
  isAddNew.value = false
  address.value = {
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  }
}
function edit(id: number){
  isAddNew.value = true
  const editedIndex = addressList.value.findIndex((item) => item.id === id)
  address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex])) //การจะก้อปปี้ต้องแปลงเป็น json เป็น string ที่แทนเป็น obj ได้ แล้วค่อยเปลี่ยน json sting เป็น obj ใหม่
  //Copy Obj -> JSON.parse(JSON.stringify(obj))
}
function remove(id: number){
  const removeIndex = addressList.value.findIndex((item) => item.id === id)
  addressList.value.splice(removeIndex,1)
}
function cancel(){
  isAddNew.value = false
  address.value = {
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  }
}

  return { address,addressList,save,isAddNew,edit,cancel,remove }
})
